# Ejemplo de reCAPTCHA 

_El objetivo de este proyecto es dar a conocer como llevar acabo la implantación del reCAPTCHA de Google, de tal manera que permita ocultar contenido la página como botones y cajas de texto hasta que el captcha sea verificado._

### Tecnologias

_Dentro de este proyecto, se utilizara una tecnologia para mejorar la experiencia del usuario._

_**CAPTCHA**, (test de Turing público y automático para distinguir a los ordenadores de los humanos, del inglés "Completely Automated Public Turing test to tell Computers and Humans Apart") es un tipo de medida de seguridad conocido como autenticación pregunta-respuesta. Un CAPTCHA te ayuda a protegerte del spam y del descifrado de contraseñas pidiéndote que completes una simple prueba que demuestre que eres humano y no un ordenador que intenta acceder a una cuenta protegida con contraseña._

_**Bootstrap**, es un framework originalmente creado por Twitter, que permite crear interfaces web con CSS y JavaScript, cuya particularidad es la de adaptar la interfaz del sitio web al tamaño del dispositivo en que se visualice. Es decir, el sitio web se adapta automáticamente al tamaño de una PC, una Tablet u otro dispositivo. Esta técnica de diseño y desarrollo se conoce como “responsive design” o diseño adaptativo._
 
_**HTTP**, Es un servidor Web es un programa que utiliza el protocolo de transferencia de hiper texto, HTTP (Hypertext Transfer Protocol), para servir los archivos que forman páginas Web a los usuarios, en respuesta a sus solicitudes, que son reenviados por los clientes HTTP de sus computadoras. Las computadoras y los dispositivos dedicados también pueden denominarse servidores Web._
## Comenzando 🚀

_Para tener una copia de este repositorio descarga el Zip o clona el repositorio con la siguiente liga: https://gitlab.com/isaac.rodriguez/recaptcha-v2.git._

Mira **Deployment** para conocer como desplegar el proyecto.


### Pre-requisitos 📋

_1.- Un servidor HTTP._

_2.- Una cuenta de Google._

_3.- Ingresar a la siguiente liga: [My captcha](https://www.google.com/recaptcha/admin#list) y dar de alta tu sitio para obtener la clave del sitio que será necesaria para la implementación del captcha._

**Ejemplo de llenado de formulario para añadir sitio.**

![La imagen muestra un ejemplo de como llenar los datos del sito para el captcha](https://gitlab.com/isaac.rodriguez/recaptcha-v2/raw/master/Im%C3%A1genes%20/Ejemplo_de_llenado.png)

### Instalación 🔧

_Una vez que tengamos instalado nuestro servidor HTTP de nuestra preferencia, añadiremos el repositorio a la raíz del servidor._


```
En el caso de XAMMP la ruta es: C:\xampp\htdocs
En el caso de Linux por lo regular la entraremos en: /var/www/html
```

_Ya clonado o copeado el repositorio modificaremos la línea #32_

```
<div style="margin-bottom: 20px;" id='recaptcha' class="g-recaptcha" data-sitekey="6LdQpHQUAAAAAMBeCmlLXRE16UwFKZy2MuCt7bUg" data-callback="onSubmit">
```
_Cambiaremos el valor de “data-sitekey” por la llave del sitio propia, en este ejemplo dejo la llave del sitio para que tenga acceso como localhost, lo ideal es que se cambie por la se generó con su cuenta de google._

![La imagen muestra un ejemplo de donde se encuetra la llave del sitio](https://gitlab.com/isaac.rodriguez/recaptcha-v2/raw/master/Im%C3%A1genes%20/KEYSITE.png)

_Una vez modificada la llave del sito accederemos en el navegador preferido como Chrome a nuestro ejemplo._

```
localhost/captcha.html 
o 
localhost/"nombre de la carpeta del repositorio"/captcha.html 

```
_Al acceder a la página de ejemplo nos mostrara lo siguiente:_

![La imagen muestra el sito](https://gitlab.com/isaac.rodriguez/recaptcha-v2/raw/master/Im%C3%A1genes%20/Captcha.png)

_Al completar el captcha nos mostrara un pequeño formulario dando por terminado el ejemplo._

![La imagen muestra formulario despues de completar captcha](https://gitlab.com/isaac.rodriguez/recaptcha-v2/raw/master/Im%C3%A1genes%20/Formulario.png)

## Ejecutando las pruebas ⚙️

_Los posibles errores que pueden seguir durante la implementación  del ejemplo se describirán en es la siguiente sección._

### Analice las pruebas end-to-end 🔩

_En caso de que el capcha no cargue correctamente verifica tu llave del sitio._

_Si ha surgido un error al añadir el sitio en el registro verifique que el sito que este especificando no tenga “Http:/” o que finalice con una “/” solo tendrá que especificar el dominio del sitio como se muestra en el ejemplo de ** Pre-requisitos**_

## Deployment 📦

_En caso de que quiera implementar no te olives de añadir la librería que contiene el captcha si como tener siempre en un “div” el captcha.  _

## Construido con 🛠️

* [recaptcha](https://www.google.com/recaptcha/intro/v3beta.html) - Sitio oficial
* [bootstrap](https://getbootstrap.com/) - Estilos

## Autor ✒️

* **Isaac Rodriguez** - *Trabajo Inicial* - [isaac.rodriguez](https://gitlab.com/isaac.rodriguez)

## Licencia 📄

Este proyecto está bajo la Licencia (MIT License) - mira el archivo [LICENSE.md](https://gitlab.com/isaac.rodriguez/recaptcha-v2/blob/master/LICENSE) para detalles

